package com.company;

public class Main {

    public static void main(String[] args) {

        Sguare sguare = new Sguare(14);
        Rectangle rectangle = new Rectangle(5, 10);
        Ellipse ellipse = new Ellipse(6, 4);
        Circle circle = new Circle(8);


        System.out.println("Периметр прямоугольника = " + rectangle.getPerimeter());
        System.out.println("Периметр квадрата = " + sguare.getPerimeter());
        System.out.println("Периметр овала = " + ellipse.getPerimeter());
        System.out.println("Периметр круга = " + circle.getPerimeter());
    }
}
