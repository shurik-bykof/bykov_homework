package com.company;


public class Rectangle extends Figure {
    public Rectangle(int a, int b) {
        super(a, b);
    }
    public double getPerimeter() {
        return a * 2 + b * 2;
    }
}

