package com.company;

import static java.lang.Math.sqrt;

public class Circle extends Ellipse {
    public Circle(double radius) {
        super(radius, radius);
    }

    public double getPerimeter() {
        return radius1 * 6.28;
    }
}

