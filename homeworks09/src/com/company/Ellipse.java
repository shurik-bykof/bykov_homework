package com.company;

import static java.lang.Math.sqrt;

public class Ellipse extends Figure {
    public Ellipse(double radius1, double radius2) {
        super(radius1, radius2);
    }
    public double getPerimeter() {
        return ((sqrt(radius1*radius1 + radius2*radius2)/2)*6.28);
    }
}
