package ru.pcs.web.homeworks28.controller;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import ru.pcs.web.homeworks28.models.Product;
import ru.pcs.web.homeworks28.repositories.ProductRepository;

@Controller
public class ProductsController {
    @Autowired
    private ProductRepository productRepository;

    @PostMapping("/products")
    public String addProduct(@RequestParam("name") String name,
                             @RequestParam("cost") Integer cost,
                             @RequestParam("amount") Integer amount) {
        Product product = Product.builder()
                .name(name)
                .cost(cost)
                .amount(amount)
                .build();
        productRepository.save(product);
        return "redirect:/products_add.html";
    }
}
