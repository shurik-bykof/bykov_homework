package ru.pcs.web.homeworks28.repositories;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;
import ru.pcs.web.homeworks28.models.Product;

import javax.sql.DataSource;
import java.util.List;

@Component
public class ProductRepositoryJdbcTemplateImpl implements ProductRepository {

    private static final String SQL_INSERT = "insert into product(name, cost, amount) values(?, ?, ?)";
    private static final String SQL_SELECT_ALL = "select * from product order by id";

    private JdbcTemplate jdbcTemplate;

    private static final RowMapper<Product> productRowMapper = (row, rowNumber) -> {
        Product product = new Product();
        int id = row.getInt("id");
        String name = row.getString("name");
        int cost = row.getInt("cost");
        int amount = row.getInt("amount");
        return new Product(id, name, cost, amount);
    };

    @Autowired
    public ProductRepositoryJdbcTemplateImpl(DataSource dataSource) {
        this.jdbcTemplate = new JdbcTemplate(dataSource);
    }

    private String sqlByPrice(int price) {
        String SQL_SELECT_PRICE = new String();
        SQL_SELECT_PRICE = "select * from product where cost > " + price;
        return SQL_SELECT_PRICE;
    }

    @Override
    public List<Product> findAll() {

        return jdbcTemplate.query(SQL_SELECT_ALL, productRowMapper);
    }

    @Override
    public List<Product> findAllByPrice(int price) {
        String SQL_SELECT_PRICE = sqlByPrice(price);
        return jdbcTemplate.query(SQL_SELECT_PRICE, productRowMapper);
    }

    @Override
    public void save(Product product) {
        jdbcTemplate.update(SQL_INSERT, product.getName(), product.getCost(), product.getAmount());
    }
}
