package ru.pcs.web.homeworks28.repositories;


import ru.pcs.web.homeworks28.models.Product;

import java.util.List;

public interface ProductRepository {
    List<Product> findAll();

    List<Product> findAllByPrice(int price);

    void save(Product product);


}
