package com.company;

public class ArrayList<T> {

    private static final int DEFAULT_SIZE = 10;

    private T[] elements;
    private int size = 0;

    public ArrayList() {
        this.elements = (T[]) new Object[DEFAULT_SIZE];
    }


    public void add(T element) {
        if (isFullArray()) {
            resize();
        }

        this.elements[size] = element;
        size++;
    }

    private void resize() {
        T[] oldElements = this.elements;
        this.elements = (T[]) new Object[oldElements.length + oldElements.length / 2];
        for (int i = 0; i < size; i++) {
            this.elements[i] = oldElements[i];
        }
    }

    private boolean isFullArray() {
        return size == elements.length;
    }


    public T get(int index) {
        if (isCorrectIndex(index)) {
            return elements[index];
        } else {
            return null;
        }
    }

    private boolean isCorrectIndex(int index) {
        return index >= 0 && index < size;
    }

    public void clear() {
        this.size = 0;
    }

    public int length() {
        return this.size;
    }


    public void removeAt(int index) {
        if (isCorrectIndex(index)) {
            for (int i = index; i < size - 1; i++) {
                elements[i] = elements[i + 1];
            }
            this.size--;
        }else {
            System.out.println("Index outside.");
        }
    }
}
