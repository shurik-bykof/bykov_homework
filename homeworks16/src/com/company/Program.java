package com.company;

public class Program {
    public static void main(String[] args) {
        ArrayList<Integer> arrayList = new ArrayList<>();
        LinkedList<Integer> linkedList = new LinkedList<>();

        for (int i = 0; i < 15; i++) {
            arrayList.add(i);
            linkedList.add(i);
        }

        System.out.println("Input arrayList:");
        for (int element = 0; element < arrayList.length(); element++) {
            System.out.print(arrayList.get(element) + ", ");
        }

        System.out.println("\nInput linkedList:");
        for (int element = 0; element < linkedList.length(); element++) {
            System.out.print(linkedList.get(element) + ", ");
        }

        for (int element = 0; element < arrayList.length(); element++) {
            if (arrayList.get(element) % 2 == 0) {
                arrayList.removeAt(element);
            }
        }

        System.out.println("\nTest. ArrayList after remove even elements:");
        for (int element = 0; element < arrayList.length(); element++) {
            if (arrayList.get(element) % 2 == 0) {
                System.out.println("Test unsuccessful.");
                return;
            } else {
                System.out.println(arrayList.get(element) + "\t-\t is odd element.");
                // end of array
                if (element == arrayList.length() - 1) {
                    System.out.println("Test successful.");
                }
            }
        }
    }
}
