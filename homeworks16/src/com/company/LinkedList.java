package com.company;


public class LinkedList<T> {
    private static class Node<T> {
        private T value;
        Node<T> next;

        public Node(T value) {
            this.value = value;
        }
    }

    private Node<T> first;
    private Node<T> last;
    private int size;

    public void add(T element) {
        Node<T> newNode = new Node<>(element);
        if (size == 0) {
            first = newNode;
        } else {
            last.next = newNode;
        }
        last = newNode;
        size++;
    }

    public void addToBegin(T element) {
        Node<T> newNode = new Node<>(element);
        if (first == null) {
            last = newNode;
        } else {
            newNode.next = first;
        }
        first = newNode;
        size++;
    }

    public T get(int index) {
        int count = 0;
        if (isCorrectIndex(index)) {
            Node<T> currNode = first;
            while (count != index) {
                currNode = currNode.next;
                count++;
            }
            return currNode.value;
        } else {
            System.out.println("List is empty");
            return null;
        }
    }

    private boolean isCorrectIndex(int index) {
        return index >= 0 && index < size;
    }

    public int length() {
        return size;
    }
}
