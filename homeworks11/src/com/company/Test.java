package com.company;

public class Test {
    public static void test() {
        Logger[] loggers = new Logger[10];

        System.out.println("Создаём 10 loggers.");
        for (int numLogger = 0; numLogger < loggers.length; numLogger++) {
            loggers[numLogger] = Logger.getLogger();
        }

        Logger curLogger = loggers[0];

        System.out.println("И проверяем, ссылаются ли они на один объект или нет.");
        for (int nextLogger = 1; nextLogger < loggers.length; nextLogger++) {

            if (curLogger == loggers[nextLogger]) {
                int indexCurLogger = nextLogger - 1;
                curLogger.log("logger[" + indexCurLogger + "]=logger[" + nextLogger + "];");
                curLogger = loggers[nextLogger];
            } else {
                System.out.println("Oops");
                break;
            }

            if (nextLogger + 1 == loggers.length) {
                System.out.println("Как видно ссылок много, а объект один, т.е. тест пройден.");
            }
        }

    }
}
