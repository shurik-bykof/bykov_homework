package com.company;

public class Ellipse extends Figure {

    private double bigRadius;
    private double smallRadius;

    public Ellipse(int coordX, int coordY, double bigRadius, double smallRadius) {
        super(coordX, coordY);
        this.bigRadius = bigRadius;
        this.smallRadius = smallRadius;
    }

    @Override
    public double getPerimeter() {
        return 4 * (Math.PI * this.smallRadius * bigRadius + (Math.pow((this.bigRadius - this.smallRadius), 2.0))) /
                (this.bigRadius + this.smallRadius);
    }
}