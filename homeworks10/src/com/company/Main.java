package com.company;

public class Main {
    public static void main(String[] args) {

        Moving[] movingFigures = new Moving[2];

        Circle circle = new Circle(0, 0, 2.0);
        Square square = new Square(0, 0, 4);

        movingFigures[0] = circle;
        movingFigures[1] = square;

        for (int i = 0; i < movingFigures.length; i++) {
            movingFigures[i].move(2, 3);
        }
    }
}