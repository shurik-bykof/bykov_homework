package com.company;


public class Square extends Rectangle implements Moving {
    private double side;

    public Square(int coordX, int coordY, double side) {
        super(coordX, coordY, side, side);
        this.side = side;
        System.out.println("Создали квадрат с координатами: x = " + this.coordX + ", y = " + this.coordY + " и стороной = " + side);
    }

    @Override
    public void move(int setCoordX, int setCoordY) {
        this.coordX = setCoordX;
        this.coordY = setCoordY;
        System.out.println("Переместили квадрат на следующие координаты: x = " + this.coordX + ", y = " + this.coordY);
    }

    @Override
    public double getPerimeter() {
        return this.side * 4;
    }
}