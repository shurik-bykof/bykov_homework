package com.company;

public abstract class Figure {

    protected int coordX;
    protected int coordY;

    public Figure(int coordX, int coordY) {
        this.coordX = coordX;
        this.coordY = coordY;
    }

    public abstract double getPerimeter();
}