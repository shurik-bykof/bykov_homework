package com.company;


import java.util.Arrays;


public class Sequence {

    public static int[] filter(int[] array, ByCondition condition) {

        int[] outArray = new int[array.length];

        //countMatchElements - количество актуальных элементов в массиве array
        int countMatchElements = 0;

        for (int numFromArray : array) {

            if (condition.isOk(numFromArray)) {
                outArray[countMatchElements] = numFromArray;
                countMatchElements++;
            }
        }

        outArray = Arrays.copyOf(outArray, countMatchElements);

        return outArray;
    }
}
