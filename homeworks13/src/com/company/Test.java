package com.company;
import java.util.Arrays;

public class Test {

    public static int[] test(int[] array) {

        System.out.println("Входной массив:");
        System.out.println(Arrays.toString(array));

        System.out.println("Массив чётных чисел:");

        int[] evenArray = new int[array.length];
        int countEvenValuesInArray = 0;

        for (int i = 0; i < array.length; i++) {
            if (Program.isEven(array[i])) {
                evenArray[countEvenValuesInArray] = array[i];
                countEvenValuesInArray++;
            }
        }
        evenArray = Arrays.copyOf(evenArray, countEvenValuesInArray);
        System.out.println(Arrays.toString(evenArray));

        System.out.println("Массив с чётной суммой цифр :");

        int countEvenSumDigits = 0;
        int[] evenSumDigits = new int[evenArray.length];

        for (int i = 0; i < evenArray.length; i++) {
            if (Program.isSumDigitsEven(evenArray[i])) {
                evenSumDigits[countEvenSumDigits] = evenArray[i];
                countEvenSumDigits++;
            }
        }
        evenSumDigits = Arrays.copyOf(evenSumDigits, countEvenSumDigits);
        System.out.println(Arrays.toString(evenSumDigits));

        return evenSumDigits;

    }
}
