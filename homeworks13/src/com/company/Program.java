package com.company;

import java.util.Arrays;
import java.util.Scanner;

public class Program {

    public static void main(String[] args) {

        boolean test = false;
        String isTest = "";

        Scanner scanner = new Scanner(System.in);

        while (!(isTest.equals("y") || isTest.equals("n"))) {
            System.out.print("Произвести тестовое задание? y / n: ");
            isTest = scanner.next();
            isTest = isTest.toLowerCase();
        }

        int[] arrayIn;

        if (isTest.equals("y")) {
            test = true;
            arrayIn = new int[]{0, 12, 64, 54, 235, 567, 543, 44, 24, 136, 44, 56};
        } else {
            arrayIn = getArray(20);
            System.out.print("Имеем массив: ");
            System.out.println(Arrays.toString(arrayIn));
        }

        int[] resultArray = Sequence.filter(arrayIn, number -> {

            if (isEven(number)) {

                return isSumDigitsEven(number);

            } else return false;
        });

        if (test) {

            int[] testResultArray = Test.test(arrayIn);
            System.out.println("Результат, используя метод filter класса Sequence:\n" + Arrays.toString(resultArray));

            if (resultArray.length == testResultArray.length) {

                if (resultArray.length == 0) {
                    System.out.println("Нет элементов выполняющих условие (Т.е. массив пуст)");
                } else {

                    for (int i = 0; i < resultArray.length; i++) {

                        if (!(resultArray[i] == testResultArray[i])) {
                            System.out.println("Тест не прошёл успешно");
                            return;
                        }
                    }
                    System.out.println("Массивы равны. Тест прошёл успешно");
                }

            } else {
                System.out.println("Тест не прошёл успешно");
            }

        } else {
            System.out.print("Результат: ");
            System.out.println(Arrays.toString(resultArray));
        }
    }

    public static int[] getArray(int size) {
        int[] array = new int[size];
        for (int i = 0; i < array.length; i++) {
            array[i] = (int) (Math.random() * 1000);
        }
        return array;
    }

    public static boolean isEven(int value) {
        return (value % 2 == 0);
    }

    public static boolean isSumDigitsEven(int value) {
        int sumDigits = 0;

        while (value != 0) {
            sumDigits += value % 10;
            value /= 10;
        }
        return isEven(sumDigits);
    }
}
