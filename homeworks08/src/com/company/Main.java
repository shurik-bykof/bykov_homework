package com.company;

public class Main {

    public static void main(String[] args) {
        Human[] humans = new Human [10];

        humans[0] = new Human();
        humans[0].setWeight(95);
        humans[0].setName("Саша");

        humans[1] = new Human();
        humans[1].setWeight(60);
        humans[1].setName("Маша");

        humans[2] = new Human();
        humans[2].setWeight(25);
        humans[2].setName("Глаша");

        humans[3] = new Human();
        humans[3].setWeight(15);
        humans[3].setName("Дана");

        humans[4] = new Human();
        humans[4].setWeight(8);
        humans[4].setName("Стеша");

        humans[5] = new Human();
        humans[5].setWeight(30);
        humans[5].setName("Леша");

        humans[6] = new Human();
        humans[6].setWeight(3);
        humans[6].setName("Муся");

        humans[7] = new Human();
        humans[7].setWeight(120);
        humans[7].setName("Валерон");

        humans[8] = new Human();
        humans[8].setWeight(75);
        humans[8].setName("Сергей");

        humans[9] = new Human();
        humans[9].setWeight(80);
        humans[9].setName("Гена");

        sortWeight(humans);
        for (int i = 0; i < humans.length; i++) {
            System.out.println(humans[i].getName() + " " + humans[i].getWeight());
        }
    }

    public static void sortWeight(Human[] humans) {
        for ( int i=0; i < humans.length; i++) {
            int minWeight = humans[i].getWeight();
            int minIndex = i;
            for (int j= i + 1; j < humans.length; j++) {
                if (humans[j].getWeight() < minWeight) {
                    minWeight = humans[j].getWeight();
                    minIndex = j;
                }
            }
            int tempWight = humans[i].getWeight();
            String tempName = humans[i].getName();
            int minimumWight = humans[minIndex].getWeight();
            String minimumName = humans[minIndex].getName();
            humans[i].setWeight(minimumWight);
            humans[i].setName(minimumName);
            humans[minIndex].setWeight(tempWight);
            humans [minIndex].setName(tempName);
        }


    }


}
